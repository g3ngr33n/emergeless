# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2


EAPI=7
PYTHON_COMPAT=( python3_{8..10} )

inherit meson python-any-r1 xdg

DESCRIPTION="Graphical console client for connecting to virtual machines"
HOMEPAGE="https://virt-manager.org/"
GIT_HASH="f0cc7103becccbce95bdf0c80151178af2bace5a"
SRC_URI="https://gitlab.com/${PN}/${PN}/-/archive/${GIT_HASH}/${PN}-${GIT_HASH}.tar.bz2 -> ${P}.tar.bz2"
S="${WORKDIR}/${PN}-${GIT_HASH}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sasl +spice -vnc -libvirtd +vte"

RDEPEND="libvirtd? ( >=app-emulation/libvirt-0.10.0[sasl?]
	app-emulation/libvirt-glib
	>=dev-libs/libxml2-2.6 )
	x11-libs/gtk+:3
	spice? ( >=net-misc/spice-gtk-0.35[sasl?,gtk3] )
	vnc? ( >=net-libs/gtk-vnc-0.5.0[sasl?,gtk3(+)] )"
DEPEND="${RDEPEND}
	dev-lang/perl
	>=dev-util/intltool-0.35.0
	virtual/pkgconfig
	spice? ( >=app-emulation/spice-protocol-0.12.15 )"

REQUIRED_USE="|| ( spice vnc )"

#PATCHES=(
#	"${FILESDIR}"/0001-hook-qmp.patch
#	)

src_configure() {
	if use vnc; then
	local emesonargs=(
		$(meson_feature vnc vnc)
	)
	fi

	if use spice; then
	local emesonargs=( 
		$(meson_feature spice spice)
	)
	fi

	if use libvirtd; then
	local emesonargs=(
		$(meson_feature libvirt libvirt)
	)
	fi

	meson_src_configure
}
